let selectedValue = "";

let dvd = document.getElementById("DVD");
let book = document.getElementById("Book");
let furniture = document.getElementById("Furniture");

let size = document.getElementById("size");

let weight = document.getElementById("weight");

let height = document.getElementById("height");
let width = document.getElementById("width");
let length = document.getElementById("length");  


const getDiv = function(div){
    const showDvd = function(){
        dvd.style.display = "block";
        book.style.display = "none";
        furniture.style.display = "none";
        size.setAttribute("required", true);
        weight.removeAttribute("required");
        height.removeAttribute("required");
        width.removeAttribute("required");
        length.removeAttribute("required");
        return 'Dvd';
    }

    const showBook = function(){
        dvd.style.display = "none";
        book.style.display = "block";
        furniture.style.display = "none";
        size.removeAttribute("required");
        weight.setAttribute("required", true);
        height.removeAttribute("required");
        width.removeAttribute("required");
        length.removeAttribute("required");
    }

    const showFurniture = function(){
        dvd.style.display = "none";
        book.style.display = "none";
        furniture.style.display = "block";
        size.removeAttribute("required");
        weight.removeAttribute("required");
        height.removeAttribute("required", true);
        width.removeAttribute("required", true);
        length.removeAttribute("required", true);
    }

    const divs = {
        Dvd: showDvd,
        Book: showBook,
        Furniture: showFurniture,
        default: function(){
            return 'unkown'
        }
    }

    return (divs[div] || divs.default)()
}

function showDiv(){
    selectedValue = document.getElementById("productType").value;

    getDiv(selectedValue);
}