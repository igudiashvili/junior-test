<?php

require_once 'libraries/Router.php';
require_once 'libraries/Controller.php';
require_once 'libraries/Database.php';

require_once 'config/config.php';

$init = new Router();