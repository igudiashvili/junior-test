<?php

abstract class Product {

    private $db;

    public function __construct(){
        
        $this->db = new Database();

    }

    abstract public function setAttribute($data);

    abstract public function getAttribute();

    public function add($data){

        $this->db->query("INSERT INTO products (sku, name, price, type, attribute) VALUES (:sku, :name, :price, :type, :attribute)");

        $this->db->bind(':sku', $data['sku']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':type', $data['type']);
        $this->db->bind(':attribute', $data['attribute']);
        
        try{
            $this->db->execute();
        } catch (PDOException $e){
            die ($e->getMessage());
        }

    }

    public function uniqueSKU($sku){

        $this->db->query('SELECT * FROM products WHERE sku = :sku');

        $this->db->bind(':sku', $sku);

        $this->db->execute();

        if($this->db->rowCount()>0){
            return false;
        } else {
            return true;
        }

    }

}