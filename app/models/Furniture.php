<?php

class Furniture extends Product {

    public function setAttribute($data){

        $attribute = $data['height'] . 'cm' . ' x ' . $data['width'] . 'cm' . ' x ' . $data['length'] . 'cm';
        $this->attribute = $attribute;
        
    }

    public function getAttribute(){

        return $this->attribute;
        
    }

}