<?php

class Book extends Product {

    public function setAttribute($data){

        $attribute = $data['weight'] . ' KG';
        $this->attribute = $attribute;
        
    }

    public function getAttribute(){

        return $this->attribute;
        
    }

}