<?php

class Dvd extends Product {

    public function setAttribute($data){

        $attribute = $data['size'] . ' MB';
        $this->attribute = $attribute;
        
    }

    public function getAttribute(){

        return $this->attribute;
        
    }

}