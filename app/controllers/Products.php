<?php

class Products extends Controller{

    public function __construct(){
        
        $this->model = $this->model('Product');

    }

    public function index(){

        $db = new Database();

        $db->query('SELECT * FROM products');

        $data = $db->resultSet();

        $this->view('products/index', $data);

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            if(isset($_POST['delete'])){
                $skus = $_POST['delete'];

                foreach($skus as $sku){
                    $db->delete($sku);
                }

                header("Refresh:0");
            }

        }

    }

    public function add(){

        $data = [
            'sku' => '',
            'name' => '',
            'price' => '',
            'type' => '',
            'attribute' => '',
            'skuError' => ''
        ];

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $data = [
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'type' => $_POST['type'],
                'size' => $_POST['size'],
                'weight' => $_POST['weight'],
                'height' => $_POST['height'],
                'width' => $_POST['width'],
                'length' => $_POST['length'],
                'skuError' => ''
            ];

            require_once '../app/models/' . $data['type'] . '.php';

            $prod = new $data['type'];
            $prod->setAttribute($data);

            $data += array('attribute' => $prod->getAttribute());

            if(!$prod->uniqueSKU($data['sku'])){
                $data['skuError'] = 'SKU must be Unique';
            } else {
                $prod->add($data);
               header("location: URLROOT");
            }

        }

        $this->view('products/add', $data);

    }

}